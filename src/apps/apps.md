
# List of pktool applications (command line utilities)

<!-- To create the current list of pktool apps, you can use:
find src/apps/ -name 'pk*.cc'|sed -e 's/.*\//- /' -e 's/\.cc//'|sort 
-->

- pkascii2img
- pkascii2ogr
- pkclassify_nn
- pkclassify_svm
- pkcreatect
- pkcrop
- pkdiff
- pkdsm2shadow
- pkdumpimg
- pkdumpogr
- pkegcs
- pkextract
- pkfillnodata
- pkfilter
- pkfs_nn
- pkfs_svm
- pkgeom
- pkgetchandelier
- pkgetmask
- \ref pkinfo
- pklas2img
- pkmosaic
- pkndvi
- pkopt_svm
- pkpolygonize
- pkreclass
- pksensormodel
- pksetchandelier
- pksetmask
- pksieve
- pkstat
- pkstatogr
- pkxcorimg